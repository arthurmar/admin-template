import { $fetch, FetchError, FetchOptions, FetchRequest } from 'ofetch';
import { toCamel, toSnake } from 'snake-camel';

export interface PaginationMeta {
  limit: number;
  total: number;
}

export interface Response<T> {
  data: T;
}

export interface CollectionResponse<T> {
  data: T[];
}

export interface PaginatedResponse<T> extends CollectionResponse<T> {
  meta: PaginationMeta;
}

const api = async <T>(request: FetchRequest, opts?: FetchOptions<'json'>) => {
  const client = $fetch.create({
    baseURL: import.meta.env.VITE_API_URL,
    headers: {
      Accept: 'application/json',
    },
  });

  const call = async <T>(
    request: FetchRequest,
    opts?: FetchOptions<'json'>,
  ): Promise<T> => {
    try {
      const result = await client(request, toSnake(opts));

      return (<unknown>toCamel(result)) as T;
    } catch (error) {
      if (error instanceof FetchError) {
        const { response } = error;

        if (response === undefined) {
          return Promise.reject(error);
        }
      }

      throw error;
    }
  };

  return await call<T>(request, opts);
};

export default api;
