import api, { Response } from '../utils/api';

export interface ProductResource {
  id: string;
  title: string;
  description: string;
  discountPercentage: number;
  rating: number;
  stock: number;
  brand: string;
  category: string;
  thumbnail: string;
  images: string[];
}

export const fetchProductsSearch = async (
  query: string,
  page: number,
  limit: number,
) =>
  await api<Response<ProductResource[]>>(`/products/search`, {
    params: {
      q: query,
      page,
      limit,
    },
  });

export const fetchProducts = async (page: number, limit: number) =>
  await api<Response<ProductResource[]>>(`/products`, {
    params: {
      page,
      limit,
    },
  });

export const fetchProduct = async (id: string) =>
  await api<Response<ProductResource>>(`/products/${id}`);
