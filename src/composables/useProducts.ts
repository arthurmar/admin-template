import { ref, Ref, UnwrapRef } from 'vue';
import {
  fetchProduct,
  fetchProducts,
  fetchProductsSearch,
  ProductResource,
} from '../../services/product';

interface UseProduct {
  products: Ref<UnwrapRef<Array<ProductResource>>>;
  product: Ref<UnwrapRef<ProductResource | null>>;
  productsLoading: Ref<boolean>;
  productLoading: Ref<boolean>;
  getProducts: (page: number, limit?: number) => Promise<void>;
  searchProducts: (
    query: string,
    page?: number,
    limit?: number,
  ) => Promise<void>;
  getProduct: (id: string) => Promise<void>;
}

const products = ref<Array<ProductResource>>([]);
const product = ref<ProductResource | null>(null);
const productsLoading = ref<boolean>(true);
const productLoading = ref<boolean>(true);

export const useProduct = (): UseProduct => {
  const getProducts = async (
    page: number = 1,
    limit: number = 10,
  ): Promise<void> => {
    productsLoading.value = true;

    try {
      const { products: data } = await fetchProducts(page, limit);

      products.value = data;
    } catch (e) {
    } finally {
      productsLoading.value = false;
    }
  };

  const searchProducts = async (
    query: string = '',
    page: number = 1,
    limit: number = 10,
  ): Promise<void> => {
    productsLoading.value = true;

    try {
      const { products: data } = await fetchProductsSearch(query, page, limit);

      products.value = data;
    } catch (e) {
    } finally {
      productsLoading.value = false;
    }
  };

  const getProduct = async (id: string): Promise<void> => {
    productLoading.value = true;

    try {
      product.value = await fetchProduct(id);
    } catch (e) {
    } finally {
      productLoading.value = false;
    }
  };

  return {
    products,
    product,
    productsLoading,
    productLoading,
    getProducts,
    searchProducts,
    getProduct,
  };
};
