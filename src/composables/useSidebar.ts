import { ref } from 'vue';

const isOpen = ref<boolean>(false);

export function useSidebar() {
  return {
    isOpen,
  };
}
