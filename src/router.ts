import type { RouteRecordRaw } from 'vue-router';
import { createRouter, createWebHistory } from 'vue-router';

import Dashboard from './views/Dashboard.vue';
import Blank from './views/Blank.vue';
import Product from './views/Product/Product.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: '/dashboard',
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
  },
  {
    path: '/lab-test',
    name: 'Lab test',
    component: Blank,
  },
  {
    path: '/appointment',
    name: 'Appointment',
    component: Blank,
  },
  {
    path: '/medicine-order',
    name: 'Medicine order',
    component: Blank,
  },
  {
    path: '/message',
    name: 'Message',
    component: Blank,
  },
  {
    path: '/payment',
    name: 'Payment',
    component: Blank,
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Blank,
  },
  {
    path: '/products/:id',
    name: 'Product',
    component: Product,
  },
];

const router = createRouter({
  history: createWebHistory('/'),
  routes,
});

export default router;
