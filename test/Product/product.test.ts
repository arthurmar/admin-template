import { expect, test } from 'vitest';
import { fetchProducts } from '../../services/product';
import { StatusCodes } from 'http-status-codes';

test.skip('Makes a GET request to fetch product list and returns the result', async () => {
  const response = await fetchProducts(1, 10);

  expect(response.status).toBe(StatusCodes.OK);
});
