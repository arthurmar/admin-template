# Admin template

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm dev
```

### Compiles and minifies for production

```
npm build
```

### Runs vite test

```
npm run test
```
